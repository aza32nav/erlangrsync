# ErlangRsync Project

Automation backups with rsync using a file with selected folders

## Warning << This is a personal project >> Warning

+ This is a first commit
+ It is still in development and need a lot of improves
+ Manage erros for example
+ Include type specifications.
+ Support to utf-8 or unicode. Not work in other languages characters(í, ü, etc)
this result in an error in rsync (_No such file or directory and error in file IO(code 11)_). 

The representation of these special characters in erlang is not the same as in 
other languages. Check this topic in erlang.

The program works but check the code for understanding its operation
and avoid errors in the backup.

The file structure is:

```
/home/user/
/run/media/user/externalStorage/
Documents/
Music/
Videos/
```

1. The first line is the origin folder
2. The second line is the destination folder
3. The next lines are the folders to be backed up

for example, this would be the first command generated:
```
rsync -rtvu --delete /home/user/Documents/ /run/media/user/externalStorage/Documents/
```

__Don't forget to put the / at the end of the folder lines and start the folder 
names to be backed up without / to avoid rsync duplication problems.__

## License

This source code is released under MIT License.
