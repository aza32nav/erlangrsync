#!/usr/bin/env escript
%% -*- coding: utf-8 -*-

main(Args) ->
    %io:format("~p~n", [Args]),
    [Argument|_] = Args,
    io:format("Filename: ~p~n", [Argument]),
    {Origin, Destination, Folders} = split_file(Argument),
    io:format("Origin: ~p~n", [Origin]),
    io:format("Destination: ~p~n", [Destination]),
    io:format("Folders: ~p~n", [Folders]),
    End = run_command(Origin, Destination, Folders),
    io:format("-----------------------------------------------~n"),
    io:format("~s~n", [End]).

split_file(Filename) ->
    {ok, BinaryContent} = file:read_file(Filename),
    SplitContent = string:split(BinaryContent, "\n", all),
    [Origin, Destination | Folders] = SplitContent,
    DepFolders = depure_folder(Folders, []),
    {binary_to_list(Origin),binary_to_list(Destination), DepFolders}.

depure_folder([], DepFolders) -> 
    DepFolders;
depure_folder([F|Res], DepFolders) ->
    Fol = binary_to_list(F),
    if
        Fol == [] ->
            depure_folder(Res, DepFolders);
        true ->
            depure_folder(Res, [Fol|DepFolders])
   end.

make_command(Or, De, Folder) ->
    "rsync -rtvu --delete " ++ Or ++ Folder ++ " " ++ De ++ Folder.

run_command(_Or, _De, []) ->
    "End Back up commands";
run_command(Or, De, Folders)->
    [Folder|Res] = Folders,
    Cmd = make_command(Or, De, Folder),
    io:format("###############################################~n"),
    io:format("~s~n",[Cmd]),
    CmdOut = os:cmd(Cmd),
    io:format("~s~n", [CmdOut]),
    run_command(Or, De, Res).
